#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/moduleparam.h>
#include<linux/init.h>
#include<linux/stat.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nitesh");
MODULE_DESCRIPTION("a simple command line in take hellow kernel program");

static int in_number = 8998;
static char* in_string = "its something";

// This description is taken from internet for my understanding
/*
 * module_param(foo, int, 0000)
 * The first param is the parameters name
 * The second param is it's data type
 * The final argument is the permissions bits,
 * for exposing parameters in sysfs (if non−zero) at a later stage.
 */
module_param(in_number, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(in_number, "special number");

module_param(in_string, charp, 0000);
MODULE_PARM_DESC(in_string, "some message");

// i wont try array parameters here


static int __init hello_init(void)
{
  printk(KERN_INFO "Hellow world, this is kernel here\n");
  printk(KERN_INFO "this is the number input %d\n",in_number);
  printk(KERN_INFO "this is the string that comes in or default %s\n", in_string);

  return 0;
}

// has to be void

static void __exit hello_exit(void)
{
  printk(KERN_INFO "Bye bye world, kernel");
}

module_init(hello_init);
module_exit(hello_exit);
