
#ifndef SWAP_IOCTL_H

#define SWAP_IOCTL_H

#include <linux/ioctl.h>

typedef struct
{
  int first, second;
  
}swapper_t;


/*
 *
 * Direction of command operation [bits 31:30] – read, write, both, or none – filled by the corresponding macro (_IOR, _IOW, _IOWR, _IO)

  Size of the command argument [bits 29:16] – computed using sizeof() with the command argument’s type – the third argument to these macros

  8-bit magic number [bits 15:8] – to render the commands unique enough – typically an ASCII character (the first argument to these macros)

  Original command number [bits 7:0] – the actual command number (1, 2, 3, …), defined as per our requirement – the second argument to these macros.
*/


// POSIX defined IOR IOWR etc standards to ensure stabilty
#define SWAP_GET_VARIABLES _IOR('s', 1, swapper_t *)
#define SWAP_RST_VARIABLES _IO('s', 2)
#define SWAP_SWP_VARIABLES _IOW('s', 3, swapper_t *)

#endif
