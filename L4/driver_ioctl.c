
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <linux/uaccess.h>

#include "driver_ioctl.h"

#define FIRST_MINOR 0
#define MINOR_CNT 1

static dev_t dev;
static struct cdev c_dev;
static struct class *cl;
static int first = 1, second = 2;

static int my_open(struct inode *i, struct file *f)
{
	return 0;
}
static int my_close(struct inode *i, struct file *f)
{
	return 0;
}



#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))

static int my_ioctl(struct inode *i, struct file *f, unsigned int cmd, unsigned long arg)

#else
  
static int my_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
#endif
{


  swapper_t s;

  switch(cmd)
  {
    case SWAP_GET_VARIABLES:
      s.first = first;
      s.second = second;
      if(copy_to_user((swapper_t *)arg, &s, sizeof(swapper_t)))
      {
        return -EACCES;
      }
      break;

    case SWAP_RST_VARIABLES:
      first = 1;
      second = 2;
      break;
    
    case SWAP_
    default:
      return -EINVAL;
  }

  return 0;
}


static struct file_operations swap_fops =
{
	.owner = THIS_MODULE,
	.open = my_open,
	.release = my_close,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
	.ioctl = my_ioctl
#else
	.unlocked_ioctl = my_ioctl
#endif
};

static int __init swap_ioctl_init(void)
{
  int ret;
  struct device *dev_ret;

  if ((ret = alloc_chrdev_region(&dev, FIRST_MINOR, MINOR_CNT, "driver_ioctl")) < 0)
  {
    return ret;
  }

  cdev_init(&c_dev, &swap_fops);
  if(( ret = cdev_add(&c_dev, dev, MINOR_CNT)) < 0)
  {
    return ret;
  }

if (IS_ERR(cl = class_create(THIS_MODULE, "char")))
	{
		cdev_del(&c_dev);
		unregister_chrdev_region(dev, MINOR_CNT);
		return PTR_ERR(cl);
	}
	if (IS_ERR(dev_ret = device_create(cl, NULL, dev, NULL, "swap")))
	{
		class_destroy(cl);
		cdev_del(&c_dev);
		unregister_chrdev_region(dev, MINOR_CNT);
		return PTR_ERR(dev_ret);
	}

  return 0;
}


static void __exit swap_ioctl_exit(void)
{
  device_destroy(cl, dev);
  class_destroy(cl);
  cdev_del(&c_dev);
  unregister_chrdev_region(dev, MINOR_CNT);
}

module_init(swap_ioctl_init);
module_exit(swap_ioctl_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("NITESH");
