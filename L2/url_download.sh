#!/bin/bash

# $1 is the file name for output
# $2 is the url

start_time=`date +%s`

echo "entry-------" > $1
echo "" >> $1

time(curl --connect-timeout 100 $2) >> $1

end_time=`date +%s` 

run_time=$(($end_time-$start_time))
echo "start_time: $start_time" >> $1
echo "end_time: $end_time" >> $1
echo "runtime: $run_time" >> $1
echo "end--------------" >> $1

