#!/bin/bash

function cv_ml_cuda()
{
  ssh -X nitesh@172.16.13.146
}

function hprcse_cuda()
{
  ssh -X nitesh@172.16.1.80
}
function iiitdm_server()
{
  ssh -X ced16i44@172.16.1.98
}

function pace_server()
{
  ssh -X nitesh@10.21.227.51
}

function rise_server()
{
  ssh -X nitesh@192.168.1.37
}

function nfs_rise_on()
{
  sudo mount nfstools:/home/rise/$1_tools /tools
  source /tools/setup.sh
  echo "rise nfs mounted sourced setup.sh"
}

function armsim()
{
  mono ~/other_compilers/armsim_linux/armsim/ARMSim.exe
}

function nfs_rise_off()
{
  sudo umount /tools
}

function rise_prac()
{
  cd ~/RISE/2019/Verification/exp/rise-intern-practice/
}

function rise_verif()
{
  cd ~/RISE/2019/Verification
}

function rise_py_uvm()
{
  cd ~/RISE/2019/Verification/py-uvm

}

function set_date()
{
  
source ~/utilScripts/set_date.sh
}

# THIS DOESNT WORK SO DONT TRYY
#function gitlab-init()
#{
 # python3 ~/gitlab-init/gitlab-init.py
#}

function rise_export()
{
  export COCOTB=/home/nitesh/RISE/2019/Verification/py-uvm/cocotb
 	export SHAKTI_HOME=/home/nitesh/RISE/2019/c-class/base-sim
}

function rc_cp()
{


# Copying all the rcs and scripts in one go!

cp ~/.bashrc ~/myrc/
cp ~/.vimrc  ~/myrc/
rsync -r --progress ~/utilScripts ~/myrc/
rsync -r --progress ~/Downloads/Fonts ~/myrc/
#rsync -r --progress ~/.config ~/myrc

echo "Copied all the rcs to the repo folder"

cd ~/myrc
git add .
git commit -m"changes made somwhere"
git push

cd -

}

function sem_7()
{
  cd "/media/nitesh/Great_Responsibility/My Stuff/Crazy Stuff/Work/seventh sem"
}

function untar()
{
  tar tf $1 | xargs -d'\n' rm -v
  tar tf $1 | sort -r | xargs -d'\n' rmdir -v

}

function add_link()
{
  echo $1 >> $2
  cat $2
}

function dump_file()
{
  #cat -> $1
#for i in {1..$3}; do cat $1 $1 > $2 ; done
 yes $1 | head -n $2 > $3
}


#alias to change directories
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias .5='cd ../../../../..'
alias bd='cd -'

#alias's to modified commands
#alias cp='cp -i'
#alias cp =cpp
alias mv='mv -i'
alias rm='rm -iv'
alias mkdir='mkdir -p'
alias ps='ps auxf'
alias ping='ping -c 10'
alias less='less -R'
alias cls='clear'
alias apt='sudo apt'
alias multitail='multitail --no-repeat -c'
alias freshclam='sudo freshclam'
alias vi='vim'
alias svi='sudo vi'
alias vis='vim "+set si"'

# Alias's for multiple directory listing commands

alias la='ls -Alh' # show hidden files
alias ls='ls -aFh --color=always' # add colors and file type extensions
alias lx='ls -lXBh' # sort by extension
alias lk='ls -lSrh' # sort by size
alias lc='ls -lcrh' # sort by change time
alias lu='ls -lurh' # sort by access time
alias lr='ls -lRh' # recursive ls
alias lt='ls -ltrh' # sort by date
alias lm='ls -alh |more' # pipe through 'more'
alias lw='ls -xAh' # wide listing format
alias ll='ls -Fls' # long listing format
alias labc='ls -lap' #alphabetical sort
alias lf="ls -l | egrep -v '^d'" # files only
alias ldir="ls -l | egrep '^d'" # directories only
alias l='ls'

alias diff='colordiff'
alias ping='ping -c 5'
