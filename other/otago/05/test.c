



#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/aio.h>
#include <linux/uaccess.h>

#include <linux/ioctl.h>
#include <linux/cdev.h>
#include <linux/device.h>




MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nitesh");
MODULE_DESCRIPTION("A template module");



int major = 0;
MODULE_PARM_DESC(major, "device major number");




int temp_open (struct inode *inode, struct file *filp)
{
	return 0;
}

int temp_release (struct inode *inode, struct file *filp)
{
	return 0;
}

ssize_t temp_read (struct file *filp, char __user *buf, size_t count,loff_t *f_pos)
{

  return 0;
}



ssize_t temp_write (struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
{
return 0;
}


long temp_ioctl (struct file *filp, unsigned int cmd, unsigned long arg)
{

	return 0;
}


struct file_operations temp_fops = {
        .owner =     THIS_MODULE,
        .read =      temp_read,
        .write =     temp_write,
        .unlocked_ioctl = temp_ioctl,
        .open =      temp_open,
        .release =   temp_release,
};



int __init temp_init_module(void) {


  size_t s = 1;
  long long int *k;
  long long int i;
  for(i =0; ;i++)
  {
    k = kmalloc(sizeof(long long int) *(s<<i), GFP_KERNEL);
    if (k==NULL){
      printk(KERN_WARNING"max reached with prev value of size:%ld \n",s<<(i-1) );
      return -1;
    }
    printk(KERN_WARNING" current size:%ld\n",s<<i);

    kfree(k);
  }



  return 0;
}

void __exit temp_exit_module(void){

}


module_init(temp_init_module);
module_exit(temp_exit_module);
