
/**
 * File: asgn1.c
 * Date: 13/03/2011
 * Author: Your Name 
 * Version: 0.1
 *
 * This is a module which serves as a virtual ramdisk which disk size is
 * limited by the amount of memory available and serves as the requirement for
 * COSC440 assignment 1 in 2012.
 *
 * Note: multiple devices and concurrent modules are not supported in this
 *       version.
 */
 
/* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version
 * 2 of the License, or (at your option) any later version.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/list.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/device.h>
#include <linux/sched.h>

#define MYDEV_NAME "asgn1"

#define MINOR_CNT 2
#define FIRST_MINOR 0



MODULE_LICENSE("GPL");
MODULE_AUTHOR("GS Nitesh");
MODULE_DESCRIPTION("COSC440 asgn1");


/**
 * The node structure for the memory page linked list.
 */ 
typedef struct page_node_rec {
  struct list_head list;
  struct page *page;
} page_node;

typedef struct asgn1_dev_t {
  dev_t dev;            /* the device */
  struct cdev *cdev;
  struct list_head mem_list; 
  int num_pages;        /* number of memory pages this module currently holds */
  size_t data_size;     /* total data size in this module */
  atomic_t nprocs;      /* number of processes accessing this device */ 
  atomic_t max_nprocs;  /* max number of processes accessing this device */
  struct kmem_cache *cache;      /* cache memory */
  struct class *class;     /* the udev class */
  struct device *device;   /* the udev device node */
} asgn1_dev;

asgn1_dev asgn1_device;


int asgn1_major = 0;                      /* major number of module */  
int asgn1_minor = 0;                      /* minor number of module */
int asgn1_dev_count = 1;                  /* number of devices */


/**
 * This function frees all memory pages held by the module.
 */
void free_memory_pages(void) {
  page_node *curr;

  struct list_head *pos;
  struct list_head *temp;
// need to iterate list backwords
//
  printk(KERN_WARNING "Started freeing pages\n");

  list_for_each_safe(pos, temp, &asgn1_device.mem_list)
  {
  
   curr = list_entry(pos,page_node, list);

   if(curr->page)
   {
     __free_page(curr->page);
   }


   list_del(pos);

   kfree(curr);
    
  } 
 
  printk(KERN_WARNING "Finished freeing pages\n");
  // 

  /* COMPLETED*/
  /**
   * Loop through the entire page list {
   *   if (node has a page) {
   *     free the page
   *   }
   *   remove the node from the page list
   *   free the node
   * }
   * reset device data size, and num_pages
   */  

  asgn1_device.num_pages = 0;

  asgn1_device.data_size = 0;


}


/**
 * This function opens the virtual disk, if it is opened in the write-only
 * mode, all memory pages will be freed.
 */
int asgn1_open(struct inode *inode, struct file *filp) {
  /* COMPLETED */
  /**
   * Increment process count, if exceeds max_nprocs, return -EBUSY
   *
   * if opened in write-only mode, free all memory pages
   *
   */

    printk(KERN_WARNING "Entered open \n");


  atomic_t temp;
  int ret;
  atomic_set(&temp, asgn1_device.max_nprocs.counter);
  //atomic_add_and_return(1, &nprocs);
  ret = atomic_sub_and_test(atomic_add_return(1, &asgn1_device.nprocs),&temp);
  if(ret)
  {
    atomic_dec(&asgn1_device.nprocs);

    printk(KERN_WARNING "number of process already maxed\n");
    return -EBUSY;
  }

if ( (filp->f_flags & O_ACCMODE) == O_WRONLY)
{
  free_memory_pages();
    printk(KERN_WARNING "Opened in WR ONLY\n");
}

  printk(KERN_WARNING "Number of processes currently using the driver:%d \n", asgn1_device.nprocs.counter);



  return 0; /* success */
}


/**
 * This function releases the virtual disk, but nothing needs to be done
 * in this case. 
 */
int asgn1_release (struct inode *inode, struct file *filp) {
  /* COMPLETED */
  /**
   * decrement process count
   */

  int ret;
  //atomic_add_and_return(1, &nprocs);
  ret = atomic_dec_and_test(&asgn1_device.nprocs);
  if(ret)
  {
    atomic_inc(&asgn1_device.nprocs);

    printk(KERN_WARNING "number of process already 0\n");
    return -EBUSY;
  }



  printk(KERN_WARNING "After a proecss exits\n Number of processes currently using the driver:%d \n", asgn1_device.nprocs.counter);


  return 0;
}


/**
 * This function reads contents of the virtual disk and writes to the user 
 */
ssize_t asgn1_read(struct file *filp, char __user *buf, size_t count,
		 loff_t *f_pos) {
  size_t size_read = 0;     /* size read from virtual disk in this function */
  size_t begin_offset;      /* the offset from the beginning of a page to
			       start reading */
  int begin_page_no = *f_pos / PAGE_SIZE; /* the first page which contains
					     the requested data */
  int curr_page_no = 0;     /* the current page number */
  size_t curr_size_read;    /* size read from the virtual disk in this round */
  size_t size_to_be_read;   /* size to be read in the current round in 
			       while loop */

  struct list_head *ptr = asgn1_device.mem_list.next;
  page_node *curr;

  /* COMPLETE ME */
  /**
   * check f_pos, if beyond data_size, return 0
   * 
   * Traverse the list, once the first requested page is reached,
   *   - use copy_to_user to copy the data to the user-space buf page by page
   *   - you also need to work out the start / end offset within a page
   *   - Also needs to handle the situation where copy_to_user copy less
   *       data than requested, and
   *       copy_to_user should be called again to copy the rest of the
   *       unprocessed data, and the second and subsequent calls still
   *       need to check whether copy_to_user copies all data requested.
   *       This is best done by a while / do-while loop.
   *
   * if end of data area of ramdisk reached before copying the requested
   *   return the size copied to the user space so far
   */

  return size_read;
}




static loff_t asgn1_lseek (struct file *file, loff_t offset, int cmd)
{
    loff_t testpos;

    size_t buffer_size = asgn1_device.num_pages * PAGE_SIZE;

    /* COMPLETE ME */
    /**
     * set testpos according to the command
     *
     * if testpos larger than buffer_size, set testpos to buffer_size
     * 
     * if testpos smaller than 0, set testpos to 0
     *
     * set file->f_pos to testpos
     */
    printk (KERN_INFO "Seeking to pos=%ld\n", (long)testpos);
    return testpos;
}


/**
 * This function writes from the user buffer to the virtual disk of this
 * module
 */
ssize_t asgn1_write(struct file *filp, const char __user *buf, size_t count,
		  loff_t *f_pos) {
  size_t orig_f_pos = *f_pos;  /* the original file position */
  size_t size_written = 0;  /* size written to virtual disk in this function */
  size_t begin_offset;      /* the offset from the beginning of a page to
			       start writing */
  int begin_page_no = *f_pos / PAGE_SIZE;  /* the first page this finction
					      should start writing to */


  size_t ret;

  int curr_page_no = 0;     /* the current page number */
  size_t curr_size_written=0; /* size written to virtual disk in this round */
  size_t size_to_be_written;  /* size to be read in the current round in 
				 while loop */
  
  struct list_head *ptr = asgn1_device.mem_list.next;
  page_node *curr;

  size_to_be_written = count; // just for initialization

  begin_offset = (*f_pos)%PAGE_SIZE;

  printk(KERN_WARNING "begin writing %s\n", buf);

  list_for_each(ptr, &asgn1_device.mem_list)
  {
    
    if(curr_page_no == begin_page_no)
    {// address to write the
      break;
    }
    curr_page_no++;
  }    
  curr = list_entry(ptr, page_node, list);

      if(!curr->page)
      {// allocating page
        curr->page = alloc_page(GFP_ATOMIC); // may be need to change it to kernel.

        if(NULL == curr->page)
        {
          printk(KERN_WARNING "Not enough memory left\n");
          return size_written;
        }
      }

      // new size to be written
      size_to_be_written = min(count - curr_size_written, PAGE_SIZE); 

    
      ret = PAGE_SIZE - copy_from_user(page_address(curr->page) + begin_offset,
                                          buf,size_to_be_written );
      
      // ret = PAGE_SIZE - number of bytes not copied
      
      if(ret ==PAGE_SIZE) // everything was copied
      {
        curr_size_written += size_to_be_written;
      }
      
      begin_offset = ret; // if the write was error we need to have this o
      

      
      size_to_be_written = min(count - curr_size_written, PAGE_SIZE); 
      
      // when the page wasnt sufficient
      while(size_to_be_written)
      {
        page_node *new = kmalloc(sizeof(page_node), GFP_ATOMIC);
       
      if(NULL == new)
        {
          printk(KERN_WARNING "Not enough memory left for kalloc\n");
          return size_written;
        }


        new->page = alloc_page(GFP_ATOMIC);
        if(NULL == new->page)
        {
          printk(KERN_WARNING "Not enough memory left for pages\n");
          return size_written;
        }

         ret = PAGE_SIZE - copy_from_user(page_address(new->page) + begin_offset,
                                          buf,size_to_be_written );
      
            // ret = PAGE_SIZE - number of bytes not copied
      
          if(ret ==PAGE_SIZE) // everything was copied
          {
            curr_size_written += size_to_be_written;
          }
          
            begin_offset = ret; // if the write was error we need to have this o


        list_add_tail(&(new->list), &asgn1_device.mem_list);

        asgn1_device.num_pages++;

        ptr = asgn1_device.mem_list.prev;

        size_to_be_written = min(count - curr_size_written, PAGE_SIZE);
      }

    


  /* COMPLETE ME */
  /*)
   * Traverse the list until the first page reached, and add nodes if necessary
   *
   * Then write the data page by page, remember to handle the situation
   *   when copy_from_user() writes less than the amount you requested.
   *   a while loop / do-while loop is recommended to handle this situation. 
   */

          printk(KERN_WARNING "Finished Writing\n");

  asgn1_device.data_size = max(asgn1_device.data_size,
                               orig_f_pos + size_written);
  return size_written;
}

#define SET_NPROC_OP 1
#define MYIOC_TYPE 'k'
#define TEM_SET_NPROC _IOW(MYIOC_TYPE, SET_NPROC_OP, int) 

/**
 * The ioctl function, which nothing needs to be done in this case.
 */
long asgn1_ioctl (struct file *filp, unsigned cmd, unsigned long arg) {
  int nr;
  int new_nprocs;
  int result;


   printk(KERN_WARNING "reached ioctl with , CMD=%d\n", cmd);
 
          printk(KERN_WARNING " max procs :%d\n", asgn1_device.max_nprocs.counter);
   if(_IOC_TYPE(cmd) != MYIOC_TYPE)
 {
   printk(KERN_WARNING "got invalid case, CMD=%d\n", cmd);
   return -EINVAL;
  }
  nr = _IOC_SIZE(cmd);
  
  switch(cmd)
  {

  case   TEM_SET_NPROC:
          if(copy_from_user(&new_nprocs,(int*)arg, nr))
          {
            printk(KERN_WARNING " failed to set max number of procs\n");
            return -EFAULT;
          }

          if(new_nprocs < asgn1_device.nprocs.counter || new_nprocs < 0)
          {

            printk(KERN_WARNING " new max nprocs is not a valid value\n");
            return -EINVAL;
          }

          atomic_set(&asgn1_device.max_nprocs, new_nprocs);

          printk(KERN_WARNING " changed the max procs to :%d\n", asgn1_device.max_nprocs.counter);

          return 0;
          break;
  default:
   printk(KERN_WARNING "got invalid case, CMD=%d\n", cmd);

   return -ENOTTY;

  }

  /* COMPLETED*/
  /** 
   * check whether cmd is for our device, if not for us, return -EINVAL 
   *
   * get command, and if command is SET_NPROC_OP, then get the data, and
     set max_nprocs accordingly, don't forget to check validity of the 
     value before setting max_nprocs
   */

  return -ENOTTY;
}


static int asgn1_mmap (struct file *filp, struct vm_area_struct *vma)
{
    unsigned long pfn;
    unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
    unsigned long len = vma->vm_end - vma->vm_start;
    unsigned long ramdisk_size = asgn1_device.num_pages * PAGE_SIZE;
    page_node *curr;
    unsigned long index = 0;

    /* COMPLETE ME */
    /**
     * check offset and len
     *
     * loop through the entire page list, once the first requested page
     *   reached, add each page with remap_pfn_range one by one
     *   up to the last requested page
     */
    return 0;
}


struct file_operations asgn1_fops = {
  .owner = THIS_MODULE,
  .read = asgn1_read,
  .write = asgn1_write,
  .unlocked_ioctl = asgn1_ioctl,
  .open = asgn1_open,
  .mmap = asgn1_mmap,
  .release = asgn1_release,
  .llseek = asgn1_lseek
};


static void *my_seq_start(struct seq_file *s, loff_t *pos)
{
if(*pos >= 1) return NULL;
else return &asgn1_dev_count + *pos;
}
static void *my_seq_next(struct seq_file *s, void *v, loff_t *pos)
{
(*pos)++;
if(*pos >= 1) return NULL;
else return &asgn1_dev_count + *pos;
}
static void my_seq_stop(struct seq_file *s, void *v)
{
/* There's nothing to do here! */
}

int my_seq_show(struct seq_file *s, void *v) {
  /* COMPLETE ME */
  /**
   * use seq_printf to print some info to s
   */
  return 0;


}


static struct seq_operations my_seq_ops = {
.start = my_seq_start,
.next = my_seq_next,
.stop = my_seq_stop,
.show = my_seq_show
};

static int my_proc_open(struct inode *inode, struct file *filp)
{
    return seq_open(filp, &my_seq_ops);
}

struct file_operations asgn1_proc_ops = {
.owner = THIS_MODULE,
.open = my_proc_open,
.llseek = seq_lseek,
.read = seq_read,
.release = seq_release,
};



/**
 * Initialise the module and create the master device
 */
int __init asgn1_init_module(void){
  int result; 

  /* COMPLETE ME */
  /**
   * set nprocs and max_nprocs of the device
   *
   * allocate major number #
   * allocate cdev, and set ops and owner field #
   * add cdev #
   * initialize the page list
   * create proc entries
   */

  // set initial values related to procs
  atomic_set(&asgn1_device.nprocs, 0);
  atomic_set(&asgn1_device.max_nprocs, 2);


    INIT_LIST_HEAD(&asgn1_device.mem_list); // head of all the page memory


  asgn1_device.cdev = (struct cdev * )kmalloc(sizeof(struct cdev)*2,GFP_ATOMIC);


  if((result = alloc_chrdev_region(&asgn1_device.dev, FIRST_MINOR, MINOR_CNT, "my_asgn"))<0)
  {
    return result;
  }

  cdev_init(&asgn1_device.cdev[0], &asgn1_fops);

  asgn1_device.cdev[0].owner = THIS_MODULE;

    if((result = cdev_add(&asgn1_device.cdev[0], asgn1_device.dev, MINOR_CNT))<0)
    {
      return result;
    }
 
  asgn1_device.class = class_create(THIS_MODULE, MYDEV_NAME);
  if (IS_ERR(asgn1_device.class)) {
  goto fail_device;
  }

  asgn1_device.device = device_create(asgn1_device.class, NULL, 
                                      asgn1_device.dev, "%s", MYDEV_NAME);
  if (IS_ERR(asgn1_device.device)) {
    printk(KERN_WARNING "%s: can't create udev device\n", MYDEV_NAME);
    result = -ENOMEM;
    goto fail_device;
  }
  
  printk(KERN_WARNING "set up udev entry\n");
  printk(KERN_WARNING "Hello world from %s\n", MYDEV_NAME);
  return 0;

  /* cleanup code called when any of the initialization steps fail */
fail_device:
  device_destroy(asgn1_device.class, asgn1_device.dev);
  class_destroy(asgn1_device.class);
  cdev_del(&asgn1_device.cdev[0]);
  unregister_chrdev_region(asgn1_device.dev, MINOR_CNT); 


   
  /* COMPLETE ME */
  /* PLEASE PUT YOUR CLEANUP CODE HERE, IN REVERSE ORDER OF ALLOCATION */


  return result;
}


/**
 * Finalise the module
 */
void __exit asgn1_exit_module(void){

  
  /* COMPLETED */
  /**
   * free all pages in the page list 
   * cleanup in reverse order
   */


    free_memory_pages();
  printk(KERN_WARNING "max procs value before ending:%d\n", asgn1_device.max_nprocs.counter);
  device_destroy(asgn1_device.class, asgn1_device.dev);
  class_destroy(asgn1_device.class);
  printk(KERN_WARNING "cleaned up udev entry\n");
  
  printk(KERN_WARNING "Good bye from %s\n", MYDEV_NAME);
}


module_init(asgn1_init_module);
module_exit(asgn1_exit_module);


