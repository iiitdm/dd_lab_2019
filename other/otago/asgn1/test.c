#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include<sys/ioctl.h>


#define SET_NPROC_OP 1

#define MYIOC_TYPE 'k'
#define TEM_SET_NPROC _IOW(MYIOC_TYPE, SET_NPROC_OP, int) 

int main()
{
  int fd;

   printf("** will open the driver now\n");

  fd = open("/dev/asgn1", O_WRONLY);

  if(fd<0)
  {
    printf("Couldnt open the driver \n");
    return -1;
  }

  printf("opened the driver\n");

  int new_maxprocs = 3; 
  ioctl(fd, TEM_SET_NPROC, &new_maxprocs);

  close(fd);

  return 0;
}
