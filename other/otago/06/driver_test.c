#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include<sys/ioctl.h>



#define MYDR_BASE 'k'
#define MYDR_RESET _IO( MYDR_BASE, 1)
#define MYDR_READ _IOR( MYDR_BASE, 2, int * )
#define MYDR_WRITE _IOW( MYDR_BASE, 3, int * )
#define MYDR_STOP _IO(  MYDR_BASE, 4)


int main()
{
  int fd;

  int ds[10];
  int i;

  for(i =0;i<10;i++)
    ds[i]=i;

  printf("** will open the driver now\n");

  fd = open("/dev/ioctl_driver", O_RDWR);

  if(fd<0)
  {
    printf("Couldnt open the driver \n");
    return -1;
  }

  printf("writing the data on to the driver\n");
  ioctl(fd, MYDR_WRITE, ds);

  printf("writing succeeded\n");

  printf("reading the from driver\n");
  ioctl(fd, MYDR_READ, ds);

  printf("the read value is :\n");

  for(i =0;i<10;i++)
    printf("%d:%d\n",i,ds[i]);
    

  return 0;
}
