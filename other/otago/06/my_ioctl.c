
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/aio.h>
#include <linux/uaccess.h>

#include <linux/ioctl.h>
#include <linux/cdev.h>
#include <linux/device.h>




MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nitesh");
MODULE_DESCRIPTION("A template module");




#define MYDR_BASE 'k'
#define MYDR_RESET _IO( MYDR_BASE, 1)
#define MYDR_READ _IOR( MYDR_BASE, 2, int * )
#define MYDR_WRITE _IOW( MYDR_BASE, 3, int * )
#define MYDR_STOP _IO(  MYDR_BASE, 4)

#define FIRST_MINOR 0
#define MINOR_CNT 1

static dev_t dev;
static struct cdev c_dev;
static struct class *cl;



static int my_driver_open(struct inode *i, struct file *f)
{
  return 0;
}

static int my_driver_close(struct inode *i, struct file *f)
{
  return 0;
}

static long my_driver_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
{

  if(_IOC_TYPE(cmd) != MYDR_BASE) return -EINVAL;


  size_t n;
  int i;
  int direction;

  int *ds ;
  
  
  ds= (int*)kmalloc(sizeof(int)*n, GFP_KERNEL);

  //direction = _IOC_DIR(cmd); since we are doing cmd by cases
  n = _IOC_SIZE(cmd);
  switch(cmd)
  {
    case MYDR_RESET:
          memset(ds, 0, n);
          break;

    case MYDR_WRITE:
          if(copy_from_user(ds, (int *) arg, n))
            {
            return -EACCES;
            }
            for(i =0;i<n;i++)
            printk(KERN_WARNING "the data at %d: %d\n", i, ds[i]);
            break;
  
   case MYDR_READ:

          memset(ds, 99, n);
            if(copy_to_user((int* )arg, &ds[0], n))
              return -EFAULT;
            break;
  
    default:
            printk(KERN_WARNING "got invalid case, CMD=%d\n", cmd);
            return -EINVAL;

  }

  if(ds)
    kfree(ds);

 return 0;

}



static struct file_operations fops =
{
  .owner = THIS_MODULE,
  .open = my_driver_open,
  .release = my_driver_close,
  .unlocked_ioctl = my_driver_ioctl
};



static int __init my_driver_init(void)
{
  int ret; 
    if((ret = alloc_chrdev_region(&dev, FIRST_MINOR, MINOR_CNT, "my_io")) < 0)
    {
      return ret;

    }

    cdev_init(&c_dev, &fops);

    if((ret = cdev_add(&c_dev, dev, MINOR_CNT))<0)
    {
      return ret;
    }

    if(IS_ERR(cl = class_create(THIS_MODULE, "my_class")))
    {
      cdev_del(&c_dev);
      unregister_chrdev_region(dev, MINOR_CNT);
      return PTR_ERR(cl);
    }

    if(IS_ERR(ret = device_create(cl, NULL, dev, NULL, "ioctl_driver")))
    {
      class_destroy(cl);
      cdev_del(&c_dev);
      unregister_chrdev_region(dev, MINOR_CNT);
      return PTR_ERR(ret);
    }

    printk(KERN_WARNING"device created\n");

    return 0;

}

static void __exit my_driver_exit(void)
{
  device_destroy(cl, dev);
      class_destroy(cl);
      cdev_del(&c_dev);
      unregister_chrdev_region(dev, MINOR_CNT);

}


module_init(my_driver_init);
module_exit(my_driver_exit);


