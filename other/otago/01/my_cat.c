#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define BUF_SIZE 100
char buf[BUF_SIZE];

size_t size_to_be_read = BUF_SIZE;


int main(int argc, char** argv)
{
  
  int eno;

  int file = open(argv[1],O_RDONLY);

  size_t size_read, size_written_this_time, size_written, size_remaining;

restart:

  while((size_read = read(file, buf, size_to_be_read)) > 0)
  {
    // reading the data and writing the data to stdout
    //
    size_remaining = size_read;
    size_written = 0;

    while((size_written_this_time = write(STDOUT_FILENO, buf,size_remaining)) < size_remaining)
    {
      if(size_written_this_time < 0)
      {
        if(EINTR == errno) {

          // interrupt so can start again
          continue;
        }

        else{
          perror("write()");
          exit(EXIT_FAILURE);
        }
      }

      else{
        /*
         * Less than required was written so need to write again and
         * update the size of the size_remaining 
         * */

        size_written += size_written_this_time;
        size_remaining -=  size_written_this_time;
      }
    }

  }

  if(size_read <0 )
  {  if(EINTR == errno)
      {
        // interupted by a signal so we can go back to re read
        goto restart;
      }
  
      else{
        perror("read()");
          exit(EXIT_FAILURE);

        }
  }

  close(file);

  return 0;

}
